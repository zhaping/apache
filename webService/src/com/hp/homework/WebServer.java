package com.hp.homework;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class WebServer {
	
	/**
	 * 专门启动web服务器
	 */
	public void serverStar(int port){
		//监听80端口
		ServerSocket serverSocket;
		try {
			serverSocket = new ServerSocket(8090);
			while(true){
				Socket socket=serverSocket.accept();
				new Processor(socket).start();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public static void main(String[] args) {
		int port=8090;
		
		if(args.length==1){
			port=Integer.parseInt(args[0]);
		}
		new WebServer().serverStar(port);
	}
}
